# Contando cuartos

Esta experiencia esta basada en http://nifty.stanford.edu/2015/tychonievich-sherriff-layer-counting-squares/


Los algoritmos son uno de los conceptos más fundamentales de Ciencia de Cómputos. Dado un pequeño conjunto de instrucciones y las estructuras básicas de programación, podemos resolver una gran cantidad de problemas. En esta experienca vamos a practicar la creación de algoritmos simulando un robot que debe explorar un espacio utilizando  un conjunto bien limitado de instrucciones.

## Objetivos

1. Diseñar algoritmos usando estructuras secuenciales, de decisión y repetición.
2. Analizar el número de pasos de los algoritmos propuestos y tratar de minimizarlos.
3. Practicar la invocación de métodos a un objeto.

## Pre-Lab

Antes de llegar al laboratorio debes haber:

1. Repasado las estructuras básicas de decisión y repetición en C++.
1. Repasado la creación de objetos e invocación de sus métodos.
1. Tomado el quiz Pre-Lab que se encuentra en Moodle.

## Robot cuenta cuartos

En este ejercicio estaremos programando un robot que ha sido puesto en una cuadrícula (grid) de habitaciones cuadradas. Cada una de las cuatro paredes de cada cuarto puede tener una puerta. Solo las paredes que colindan con otros cuartos tienen puertas. Las paredes que forman la parte exterior de la cuadrícula o que colindan con huecos no tienen puertas.

![](http://i.imgur.com/bo6OnWf.png)

**Figura 1.** El robot está en el cuarto en el extremo superior izquierdo de la cuadrícula de 36 cuartos.

Los únicos comandos que entiende el robot son:

1. Verificar si hay una puerta en la pared del norte (N), sur(S), este(E) u oeste(O) del cuarto donde se encuentra.
2. Moverse al cuarto que queda justo al norte (N), sur(S), este(E) u oeste(O) del cuarto actual.
4. Crear variables y asignarle valores.
3. Realizar operaciones suma, resta, multiplicación y resta.
5. Usar estructuras de decisión y repetición.
6. Desplegar resultados a pantalla.


## Objetos y métodos

A continuación mostramos la función main de un programa básico como el que estarás creando. Durante este lab solo tienes que programar la función `main` (dentro del archivo main.cpp). Los demás archivos contienen funciones que implementan la funcionalidad de las instrucciones que entiende el robot.


```cpp
int main(int argc, char *argv[]) {
    QApplication   a(argc, argv);

    // Crear la cuadrícula y robot
    MainGameWindow *w = new MainGameWindow(Mode::RECT_RANDOM);
    w->show();

    // Mostrar la palabra "Start"
    w->display("Start");

    // Mover el robot hacia el oeste cuanto se pueda
    // mientras contamos el numero de movidas

    int ctr = 0;
    while (w->canMove('W')) {
        w->moveRobot('W');
        ctr = ctr + 1;
    }

    // Desplegar el total de movidas
    w->display("Total: " + QString::number(ctr));

    return a.exec();
}

```

**Figura 2** Ejemplo de una función `main`.

En el ejemplo estamos creando un robot dentro de un espacio cuadrado que solo sabe verificar si hay una puerta hacia el oeste y (si hay puerta) caminar hacia esa misma dirección.  Veamos la función, línea por línea.

La primera línea crea el único objeto que debes crear, un objeto de clase `MainGameWindow`. El parámetro `Mode::SQUARE_TOP_LEFT` especifica que la cuadrícula será cuadrada y que el robot comenzará en la esquina superior izquierda. Otras opciones para el parámetro son `RECT_TOP_LEFT`, `RECT_RANDOM` y `PYRAMID_RANDOM`.

```cpp
MainGameWindow *w = new MainGameWindow(Mode::SQUARE_TOP_LEFT);
```

La siguiente es para mostrar el objeto `w`.

```cpp
w->show();
```

El método `void display(QString)` sirve para desplegar mensajes cortos a la pantalla. Por ejemplo:

```cpp
w->display("Start");
```

, muestra la palabra "Start" antes de comenzar a mover el robot. El pedazo de código que le sigue:

```cpp
int ctr = 0;
while ( w->canMove('W') ) {
    w->moveRobot('W');
    ctr = ctr + 1;
}
```

ilustra el uso de los métodos `bool canMove(char)` y `void moveRobot(char)`:

* `bool canMove(char)` - acepta como parámetro una de las siguientes letras: `'N'`, `'S'`, `'E'` o `'W'` y devuelve `true` si existe una puerta en esa dirección del cuarto donde se encuentra el robot.
* `void moveRobot(char)` - acepta como parámetro una de las letras `'N'`, `'S'`, `'E'` o `'W'` y mueve el robot al cuarto próximo que se encuentra en esa dirección.

En el ejemplo, el código está tratando de mover al robot todas las veces que pueda hacia el oeste (`W`) y contándolas.



## Sesión de laboratorio

### Ejericio 1 - Cuadrícula cuadrada de cuartos

Supón que el robot se encuentra en el cuarto superior izquierdo (extremo noroeste) de un espacio **cuadrado** de cuartos, i.e. el espacio contiene igual número de filas y columnas de cuartos (como el de la Fiugra 1). Diseña un algoritmo para que el robot pueda computar el número de cuartos que hay en la cuadrícula.

#### Instrucciones

1. Descarga el repositorio ???? a tu computadora.

2. Carga a QtCreator el proyecto ????? haciendo doble "click" en el archivo ????? dentro del directorio que acabas de descargar (????).

3. Recuerda, durante este lab solo tienes que programar la función `main` (dentro del archivo main.cpp). Los demás archivos contienen funciones que implementan la funcionalidad de las instrucciones que entiende el robot.

4. Al implementar tu algoritmo debes asegurarte que el objeto `MainGameWindow` es creado usando el argumento `Mode::SQUARE_TOP_LEFT`.  Recuerda, el robot no sabe de antemano cuantos cuartos hay. Implementa el algoritmo y pruébalo.

5. Si el tamaño de la cuadrícula es 3x3, ¿cuántos cuartos debe visitar el robot para completar tu algoritmo?. ¿Qué tal 4x4? ¿Qué tal $n \times n$ cuartos?

6. Presume que deseamos ahorrar en la energía que utiliza el robot. ¿Puedes hacer un algoritmo que utilice menos movidas para el mismo tamaño de cuadrícula?

7. Una vez hayas terminado el algoritmo, lo hayas hecho correcto y eficiente, somételo. En el encabezado del programa escribe y explica la expresión que hayaste sobre cuántos cuartos debe visitar el robot para completar su tarea para una cuadrícula $n \times n$ (algo así como "El robot toma 2n+5 movidas. 5 para llegar al medio y 2n para contar el resto")


### Ejericio 2 -  Cuadrícula rectangular de cuartos

Supón que ahora el robot se encuentra en el cuarto superior izquierdo (extremo noroeste) de un espacio **rectangular**  (no necesariamente cuadrado) de cuartos. Diseña un algoritmo para que el robot pueda computar el número de cuartos que hay en la cuadrícula.

1. Para probar esta parte en programación debes asegurarte que objeto `MainGameWindow` es creado usando el argumento `Mode::RECT_TOP_LEFT`.

2. Una vez hayas terminado el algoritmo, lo hayas hecho correcto y eficiente, somételo. En el encabezado del programa escribe y explica la expresión que hayaste sobre cuántos cuartos debe visitar el robot para completar su tarea en una cuadrícula mxn.

### Ejericio 3 - Cuadrícula rectangular de cuartos, posición aleatoria

1. Supón que ahora el robot comienza su recorrido en cualquiera de los cuartos de una cuadrícula **rectangular** (no necesariamente cuadrada).  Diseña un algoritmo para que el robot pueda computar el número de cuartos que hay en la cuadrícula.

2. Para probar esta parte en programación debes asegurarte que objeto `MainGameWindow` es creado usando el argumento Mode::RECT_RANDOM.

3. Una vez hayas terminado el algoritmo, lo hayas hecho correcto y eficiente, somételo. En el encabezado del programa escribe y explica la expresión que hayaste sobre cuántos cuartos debe visitar el robot para completar su tarea en una cuadrícula $m \times n$. En este caso, el número de cuartos a visitar va a depender de la posición inicial del robot, así que expresa el peor de los casos, i.e. ¿cuántos cuartos debe visitar tu algoritmo si el robot comienza en el *peor* de los cuartos.



### Ejericio 4 -  Cuadrícula en forma de pirámide, posición aleatoria

1. Supón que ahora el robot comienza su recorrido en cualquiera de los cuartos de una cuadrícula en forma de piramide.  Diseña un algoritmo para que el robot pueda computar el número de cuartos que hay en la cuadrícula.

2. Para probar esta parte en programación debes asegurarte que objeto `MainGameWindow` es creado usando el argumento Mode::PYRAMID_RANDOM.

3. Una vez hayas terminado el algoritmo, lo hayas hecho correcto y eficiente, somételo. En el encabezado del programa escribe y explica la expresión que hayaste sobre cuántos cuartos debe visitar el robot para completar su tarea en una cuadrícula mxn. En este caso, el número de cuartos a visitar va a depender de la posición inicial del robot, así que expresa el peor de los caso, i.e. ¿cuántos cuartos visitaría tu algoritmo si el robot comienza en el *peor* de los cuartos.






The robot knows how to

Check N (or Check E, or S, or W) to see if there is a door on that wall of its current room;
Go N (or E or S or W) to move one room over; and
Do basic math and remember numbers.
If you ask it to Go through a wall that does not have a door, it isn’t smart enough to know it can’t so it will walk into the wall and damage itself.

We won’t be super formal in this lab. If we can tell what you are asking the robot to do, that’s good enough for us.




For this lab we will be programming a robot that has been dropped into a grid of square rooms. Each wall of each room has been painted a different color: the North wall is Neon, the East wall is Eggplant, the South wall is Sandy, and the West wall is Wallnut. Walls between rooms have doors in them, but walls on the edge do not. All of the doors are closed initially.
